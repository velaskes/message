//
//  CreateChatViewModel.swift
//  Message
//
//  Created by Slava Bulgakov on 22/05/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation
import ReactiveSwift

class CreateChatViewModel {
    var appController: AppController?
    fileprivate let token: Lifetime.Token
    fileprivate let lifetime: Lifetime
    
    init() {
        token = Lifetime.Token()
        lifetime = Lifetime(token)
    }
    
    func createChat(login: String, completion: @escaping (Bool) -> Void) {
        appController?.network?.createChat(login: login).take(during: lifetime).startWithResult({ result in
            if case .success(_) = result {
                completion(true)
                return
            }
            completion(false)
        })
    }
}
