//
//  CreateChatViewController.swift
//  Message
//
//  Created by Slava Bulgakov on 22/05/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import UIKit

class CreateChatViewController: UIViewController, Coordinated {
    @IBOutlet weak var loginTextField: UITextField!
    var viewModel: CreateChatViewModel?
    var coordinationDelegate: CoordinationDelegate?

    @IBAction func createButtonTap(_ sender: UIButton) {
        guard let login = loginTextField.text else { return }
        viewModel?.createChat(login: login) { [weak self] success in
            if success {
                self?.dismiss(animated: true, completion: nil)
                return
            }
            let alert = UIAlertController(title: "Cannot create chat", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
    }
}
