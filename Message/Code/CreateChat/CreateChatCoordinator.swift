//
//  CreateChatCoordinator.swift
//  Message
//
//  Created by Slava Bulgakov on 22/05/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation

class CreateChatCoordinator: Coordinator {
    var appController: AppController
    let createChatViewController: CreateChatViewController
    
    init(createChatViewController: CreateChatViewController, appController: AppController) {
        self.appController = appController
        self.createChatViewController = createChatViewController
    }
    
    func start() {
        let viewModel = CreateChatViewModel()
        viewModel.appController = appController
        createChatViewController.viewModel = viewModel
    }
}
