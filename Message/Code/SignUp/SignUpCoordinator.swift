//
//  SignUpCoordinator.swift
//  Message
//
//  Created by Slava Bulgakov on 17/11/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import UIKit

class SignUpCoordinator: Coordinator {
    var appController: AppController
    let signUpViewController: SignUpViewController
    
    init(signUpViewController: SignUpViewController, appController: AppController) {
        self.signUpViewController = signUpViewController
        self.appController = appController
    }
    
    func start() {
        let viewModel = SignUpViewModel(appController: appController)
        signUpViewController.viewModel = viewModel
        signUpViewController.coordinationDelegate = self
    }
}

extension SignUpCoordinator: CoordinationDelegate {
    func prepareForSegue(segue: UIStoryboardSegue) {
        
    }
}
