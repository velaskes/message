//
//  SignUpViewController.swift
//  Message
//
//  Created by Slava Bulgakov on 17/11/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class SignUpViewController: UIViewController, Coordinated {
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    fileprivate var disposable: ScopedDisposable<AnyDisposable>?
    var coordinationDelegate: CoordinationDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    var viewModel: SignUpViewModel? {
        didSet { bindViewModel() }
    }

    func bindViewModel() {
        let composite = CompositeDisposable()
        defer {
            disposable = ScopedDisposable(composite)
        }
        
        guard let viewModel = self.viewModel else { return }
        composite += viewModel.alertSignal.take(during: self.reactive.lifetime).observeValues { [weak self] alertModel in
            let alert = UIAlertController(model: alertModel, preferresStyle: .alert)
            self?.present(alert, animated: true, completion: nil)
        }
        
        composite += viewModel.signUpSuccessSignal.take(during: self.reactive.lifetime).observeValues { [weak self] _ in
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func signUpButtonTap(_ sender: UIButton) {
        guard let login = loginField.text, let password = passwordField.text else { return }
        viewModel?.signUp(login: login, password: password)
    }
}
