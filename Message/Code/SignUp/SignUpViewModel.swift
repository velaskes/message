//
//  SignUpViewModel.swift
//  Message
//
//  Created by Slava Bulgakov on 17/11/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Result

class SignUpViewModel {
    let appController: AppController
    fileprivate let signUpSuccessObserver: Observer<Void, NoError>
    let signUpSuccessSignal: Signal<Void, NoError>
    fileprivate let alertObserver: Observer<ActionMenuModel, NoError>
    let alertSignal: Signal<ActionMenuModel, NoError>
    
    init(appController: AppController) {
        self.appController = appController
        (signUpSuccessSignal, signUpSuccessObserver) = Signal<Void, NoError>.pipe()
        (alertSignal, alertObserver) = Signal<ActionMenuModel, NoError>.pipe()
    }
    
    func signUp(login: String, password: String) {
        appController.network?.signUp(login: login, password: password)?.startWithResult({ [weak self] result in
            switch result {
            case .success:
                self?.signUpSuccessObserver.send(value: Void())
            case .failure(let error):
                self?.alertObserver.send(value: error.menuModel())
            }
        })
    }
}

fileprivate extension NetworkController.NetworkError {
    func menuModel() -> ActionMenuModel {
        switch self {
        case .loginIsAlreadyExist:
            let actionMenuModel = ActionMenuModel(title: "Ошибка", message: "Данный логин уже существует.")
            actionMenuModel.addAction(ActionMenuAction(title: "OK", style: .default, image: nil, enabled: true, handler: nil))
            return actionMenuModel
        default:
            let actionMenuModel = ActionMenuModel(title: "Неизвестная ошибка", message: nil)
            actionMenuModel.addAction(ActionMenuAction(title: "OK", style: .cancel, image: nil, enabled: true, handler: nil))
            return actionMenuModel
        }
    }
}
