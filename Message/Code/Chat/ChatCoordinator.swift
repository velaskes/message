//
//  ChatCoordinator.swift
//  Message
//
//  Created by Slava Bulgakov on 20/05/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation

class ChatCoordinator: Coordinator {
    var appController: AppController
    let chatViewController: ChatViewController
    let chat: Chat
    
    init(chatViewController: ChatViewController, appController: AppController, chat: Chat) {
        self.appController = appController
        self.chatViewController = chatViewController
        self.chat = chat
    }
    
    func start() {
        let viewModel = ChatViewModel(chat: chat)
        viewModel.appController = appController
        chatViewController.viewModel = viewModel
    }
}
