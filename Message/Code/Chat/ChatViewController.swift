//
//  ViewController.swift
//  Message
//
//  Created by Slava Bulgakov on 27/02/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var allTextView: UITextView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var enterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.load()
    }
    
    var viewModel: ChatViewModel? {
        didSet { bindViewModel() }
    }
    
    func bindViewModel() {
        viewModel?.messageSignal()?.take(during: reactive.lifetime).observeValues({ [weak self] message in
            self?.allTextView.text = "\(String(describing: self?.allTextView.text ?? ""))\n\(message)"
        })
    }
    
    @IBAction func sendButtonTap(_ sender: UIButton) {
        guard let text = typeTextField.text, !text.isEmpty else { return }
        viewModel?.send(text: text)
    }
}
