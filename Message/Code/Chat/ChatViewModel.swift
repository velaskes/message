//
//  ChatViewModel.swift
//  Message
//
//  Created by Slava Bulgakov on 22/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

class ChatViewModel {
    var appController: AppController?
    fileprivate var socket = MutableProperty<WebSocketWrapper?>(nil)
    fileprivate var chat: Chat
    
    fileprivate let token: Lifetime.Token
    fileprivate let lifetime: Lifetime
    
    
    
    init(chat: Chat) {
        self.chat = chat
        token = Lifetime.Token()
        lifetime = Lifetime(token)
    }
    
    func load() {
        guard let networkConfig = appController?.settings?.currentNetworkConfig else {
            print("appController is nil")
            return
        }
        socket.value = WebSocketWrapper(config: networkConfig)
        socket.value?.connectSignal.take(during: lifetime).observeValues { [weak self] isConnect in
            guard isConnect else { return }
            let enter = Enter()
            enter.chatId = self?.chat.id
            enter.userLogin = self?.appController?.session?.user?.login
            self?.socket.value?.send(enter: enter)
        }
        socket.value?.connect()
    }
    
    func send(text: String) {
        let message = Message()
        message.text = text
        message.chatId = chat.id
        message.userLogin = self.appController?.session?.user?.login
        socket.value?.send(message: message)
    }
    
    func messageSignal() -> Signal<String, NoError>? {
        return socket.signal.skipNil().flatMap(.latest) { socket in
            socket.receiveSignal.map{ $0.text }
        }
    }
}
