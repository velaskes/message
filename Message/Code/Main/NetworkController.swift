//
//  NetworkController.swift
//  Message
//
//  Created by Slava Bulgakov on 12/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Result
import Alamofire


class NetworkController {
    enum NetworkError: Error {
        case parsing
        case url
        case signUpNeeded
        case signInNeeded
        case wrongPassword
        case loginDontExist
        case wrongAccessToken
        case serverError
        case unknownError
        case loginIsAlreadyExist
        
        init(errorResponse: ErrorResponse) {
            switch errorResponse.errorType {
            case .signUpNeeded:
                self = .signUpNeeded
            case .signInNeeded:
                self = .signInNeeded
            case .wrongPassword:
                self = .wrongPassword
            case .loginDontExist:
                self = .loginDontExist
            case .wrongAccessToken:
                self = .wrongAccessToken
            case .serverError:
                self = .serverError
            case .unknownError:
                self = .unknownError
            case .loginIsAlreadyExist:
                self = .loginIsAlreadyExist
            }
        }
    }
    
    let config: NetworkConfig
    let session: SessionController
    
    init(config: NetworkConfig, session: SessionController) {
        self.config = config
        self.session = session
    }
    
    func signIn(login: String, password: String) -> SignalProducer<Tokens, NetworkError>? {
        let credentials = Credentials(login: login, md5Password: password.md5())
        guard let data = try? commandFamilyRule.dumpData(credentials) else { return nil }
        return request(withEndpoint: .signIn, postData: data).on(failed: { _ in self.session.user = nil }, value: {
            self.session.user = CurrentUser(login: login, password: password,
                                            accessToken: $0.accessToken, refreshToken: $0.refreshToken)
        })
    }
    
    func signUp(login: String, password: String) -> SignalProducer<Tokens, NetworkError>? {
        let credentials = Credentials(login: login, md5Password: password.md5())
        guard let data = try? commandFamilyRule.dumpData(credentials) else { return nil }
        return request(withEndpoint: .signUp, postData: data).on(failed: { _ in self.session.user = nil }, value: {
            self.session.user = CurrentUser(login: login, password: password,
                                            accessToken: $0.accessToken, refreshToken: $0.refreshToken)
        })
    }
    
    func createChat(login: String) -> SignalProducer<Chat, NetworkError> {
        return request(withEndpoint: .createChat, query: "user_login=\(login)")
    }
    
    func chats() -> SignalProducer<Chats, NetworkError> {
        return request(withEndpoint: .chats)
    }
    
    func request<T>(withEndpoint endpoint: Endpoint, query: String? = nil, postData: Data? = nil) -> SignalProducer<T, NetworkError> {
        return SignalProducer<T, NetworkError> { [unowned self] observer, _ in
            guard let request = URLRequest.request(withConfig: self.config, endpoint: endpoint, accessToken: self.session.user?.accessToken, query: query, postData: postData) else {
                observer.send(error: .url)
                return
            }
            Alamofire.request(request).response { response in
                guard let data = response.data, let command = try? commandFamilyRule.validateData(data) else {
                    observer.send(error: .parsing)
                    return
                }
                switch command {
                case let errorResponse as ErrorResponse:
                    observer.send(error: NetworkError(errorResponse: errorResponse))
                case let specificCommand as T:
                    observer.send(value: specificCommand)
                default:
                    observer.send(error: .parsing)
                }
            }
        }.flatMapError({ networkError in
            switch networkError {
            case .wrongAccessToken:
                guard let user = self.session.user else { return SignalProducer.empty }
                let refreshToken = RefreshToken(refreshToken: user.refreshToken)
                guard let data = try? commandFamilyRule.dumpData(refreshToken) else { return SignalProducer(error: .parsing) }
                return self.request(withEndpoint: .refreshToken, postData: data)
            case .signInNeeded:
                self.session.user = nil
                fallthrough
            default:
                return SignalProducer(error: networkError)
            }
        })
    }
}

extension URLRequest {
    static func request(withConfig config: NetworkConfig, endpoint: Endpoint, accessToken: String?, query: String? = nil, postData: Data? = nil) -> URLRequest? {
        var fullUrlString = "\(config.apiUrlString)/\(endpoint.rawValue)"
        if let query = query {
            fullUrlString = "\(fullUrlString)?\(query)"
        }
        var headers = HTTPHeaders()
        if let accessToken = accessToken {
            headers["accessToken"] = accessToken
        }
        let method: HTTPMethod = postData == nil ? .get : .post
        var request = try? URLRequest(url: fullUrlString, method: method, headers: headers)
        if let postData = postData {
            request?.httpBody = postData
        }
        return request
    }
}
