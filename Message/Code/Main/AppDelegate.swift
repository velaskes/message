//
//  AppDelegate.swift
//  Message
//
//  Created by Slava Bulgakov on 27/02/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        guard let notNilWindow = window else { return true }
        appCoordinator = AppCoordinator(window: notNilWindow)
        appCoordinator.start()
        return true
    }

}

