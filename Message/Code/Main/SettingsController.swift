//
//  SettingsController.swift
//  Message
//
//  Created by Slava Bulgakov on 12/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation

enum WebProtocol: String {
    case HTTP = "http"
    case WS = "ws"
}

struct NetworkConfig {
    let apiHost: String
    let apiVersion: String
    
    var apiUrlString: String {
        return apiUrlString(webProtocol: .HTTP)
    }
    
    func apiUrlString(webProtocol: WebProtocol) -> String {
        return "\(webProtocol.rawValue)://\(apiHost)/v\(apiVersion)"
    }
}

class SettingsController {
    fileprivate let settings: UserDefaults
    
    init(settings: UserDefaults) {
        self.settings = settings
    }
    
    var currentNetworkConfig: NetworkConfig {
        guard let key = settings.string(forKey: "SelectedConfig"),
            let dictionary = settings.dictionary(forKey: "NetworkConfig") as? [String: [String: String]],
            let apiHost = dictionary[key]?["apiHost"],
            let apiVersion = dictionary[key]?["apiVersion"]
            else {
                print("Current network config loading error")
                return NetworkConfig(apiHost: "", apiVersion: "")
        }
        return NetworkConfig(
            apiHost: apiHost,
            apiVersion: apiVersion
        )
    }
}
