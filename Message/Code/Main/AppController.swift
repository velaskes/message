//
//  AppController.swift
//  Message
//
//  Created by Slava Bulgakov on 12/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation

class AppController {
    var settings: SettingsController?
    var network: NetworkController?
    var session: SessionController?
    
    func load() {
        if let path = Bundle(for: AppDelegate.self).path(forResource: "Settings", ofType: "plist"), let settings = NSDictionary(contentsOfFile: path) as? [String : AnyObject] {
            UserDefaults.standard.register(defaults: settings)
        }
        settings = SettingsController(settings: UserDefaults.standard)
        session = SessionController()
        if let networkConfig = settings?.currentNetworkConfig, let session = session {
            network = NetworkController(config: networkConfig, session: session)
        }
    }
}
