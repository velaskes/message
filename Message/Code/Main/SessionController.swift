//
//  SessionController.swift
//  Message
//
//  Created by Slava Bulgakov on 26/07/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

class CurrentUser: User {
    var password: String
    var accessToken: String
    var refreshToken: String
    
    init(login: String, password: String, accessToken: String, refreshToken: String) {
        self.password = password
        self.accessToken = accessToken
        self.refreshToken = refreshToken
        super.init()
        self.login = login
    }
}

class SessionController {
    struct Constants {
        static let login = "login"
        static let password = "password"
        static let accessToken = "accessToken"
        static let refreshToken = "refreshToken"
        static let service = "Message"
    }
    fileprivate let userChangedObserver: Observer<CurrentUser?, NoError>
    fileprivate let userChangedSignal: Signal<CurrentUser?, NoError>
    
    
    init() {
        (userChangedSignal, userChangedObserver) = Signal<CurrentUser?, NoError>.pipe()
    }
    
    fileprivate lazy var keychainAccessToken: KeychainPasswordItem = {
        return KeychainPasswordItem(service: Constants.service, account: Constants.accessToken)
    }()
    
    fileprivate lazy var keychainRefreshToken: KeychainPasswordItem = {
        return KeychainPasswordItem(service: Constants.service, account: Constants.refreshToken)
    }()
    
    fileprivate lazy var keychainPassword: KeychainPasswordItem = {
        return KeychainPasswordItem(service: Constants.service, account: Constants.password)
    }()
    
    fileprivate(set) lazy var userProperty: Property<CurrentUser?> = {
        return Property(initial: self.user, then: self.userChangedSignal)
    }()
    
    var user: CurrentUser? {
        get {
            guard let login = UserDefaults.standard.string(forKey: Constants.login),
                let accessToken = try? keychainAccessToken.readPassword(),
                let refreshToken = try? keychainRefreshToken.readPassword(),
                let password = try? keychainPassword.readPassword(),
                !accessToken.isEmpty && !refreshToken.isEmpty && !password.isEmpty else { return nil }
            return CurrentUser(login: login, password: password, accessToken: accessToken, refreshToken: refreshToken)
        }
        
        set {
            do {
                userChangedObserver.send(value: newValue)
                guard let notNilNewValue = newValue else {
                    UserDefaults.standard.removeObject(forKey: Constants.login)
                    try keychainPassword.deleteItem()
                    try keychainAccessToken.deleteItem()
                    try keychainRefreshToken.deleteItem()
                    return
                }
                UserDefaults.standard.set(notNilNewValue.login, forKey: Constants.login)
                UserDefaults.standard.synchronize()
                try keychainPassword.savePassword(notNilNewValue.password)
                try keychainAccessToken.savePassword(notNilNewValue.accessToken)
                try keychainRefreshToken.savePassword(notNilNewValue.refreshToken)
            } catch {
                print("Keychain changing error: \(error)")
            }
        }
    }
}
