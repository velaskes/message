//
//  WebSocketWrapper.swift
//  Message
//
//  Created by Slava Bulgakov on 31/05/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation
import Starscream
import ReactiveSwift
import Result

class WebSocketWrapper {
    var socket: WebSocket?
    
    fileprivate let receiveObserver: Observer<Message, NoError>
    let receiveSignal: Signal<Message, NoError>
    
    fileprivate let connectObserver: Observer<Bool, NoError>
    let connectSignal: Signal<Bool, NoError>
    
    
    
    init(config: NetworkConfig) {
        (receiveSignal, receiveObserver) = Signal<Message, NoError>.pipe()
        (connectSignal, connectObserver) = Signal<Bool, NoError>.pipe()
        guard let url = URL(string: "\(config.apiUrlString(webProtocol: .WS))/ws") else {
            print("Cannot init WebSocketWrapper due url")
            return
        }
        socket = WebSocket(url: url)
        socket?.delegate = self
    }
    
    func connect() {
        socket?.connect()
    }
    
    func send(message: Message) {
        guard let data = try? commandFamilyRule.dumpData(message) else {
            print("Wrong sending message format")
            return
        }
        socket?.write(data: data)
    }
    
    func send(enter: Enter) {
        guard let data = try? commandFamilyRule.dumpData(enter) else {
            print("Wrong sending message format")
            return
        }
        socket?.write(data: data)
    }
}

extension WebSocketWrapper: WebSocketDelegate {
    func websocketDidReceiveMessage(socket: WebSocket, text: String) {}

    func websocketDidConnect(socket: WebSocket) {
        print("connect")
        connectObserver.send(value: true)
    }
    
    func websocketDidDisconnect(socket: WebSocket, error: NSError?) {
        print("disconnect")
        connectObserver.send(value: false)
    }
    
    func websocketDidReceiveData(socket: WebSocket, data: Data) {
        guard let message = (try? commandFamilyRule.validateData(data)) as? Message else {
            print("Wrong receiving message format")
            return
        }
        receiveObserver.send(value: message)
    }
}
