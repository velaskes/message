//
//  ActionMenu.swift
//  Message
//
//  Created by Slava Bulgakov on 12/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation
import ReactiveSwift

public typealias ActionMenuActionStyle = UIAlertActionStyle

open class ActionMenuAction {
    open let image: UIImage?
    open let title: String?
    open let style: ActionMenuActionStyle
    open var enabled: Bool
    open let handler: ((ActionMenuAction) -> Void)?

    public init(title: String? = nil, style: ActionMenuActionStyle = .default, image: UIImage? = nil, enabled: Bool = true, handler: ((ActionMenuAction) -> Void)? = nil) {
        self.image = image
        self.title = title
        self.style = style
        self.handler = handler
        self.enabled = enabled
    }
}

public protocol ActionMenuModelProtocol {
    var title: String? { get }
    var message: String? { get }
    var actions: [ActionMenuAction] { get }

    var preferredAction: ActionMenuAction? { get }

    func addAction(_ action: ActionMenuAction)
    var close: (() -> Void)? { get set }
}

open class ActionMenuModel: ActionMenuModelProtocol {

    open var close: (() -> Void)?
    open let title: String?
    open let message: String?
    open fileprivate(set) var actions = [ActionMenuAction]()
    open var preferredAction: ActionMenuAction?

    public init(title: String?, message: String?) {
        self.title = title
        self.message = message
    }

    open func addAction(_ action: ActionMenuAction) {
        actions.append(action)
    }
}

extension UIAlertController {

    public convenience init(model: ActionMenuModelProtocol, preferresStyle: UIAlertControllerStyle) {

        self.init(title: model.title, message: model.message, preferredStyle: preferresStyle)

        for action in model.actions {

            let alertAction = UIAlertAction(title: action.title, style: action.style, handler: { _ in action.handler?(action) })
            addAction(alertAction)

            if model.preferredAction === action {
                preferredAction = alertAction
            }
        }
    }
}
