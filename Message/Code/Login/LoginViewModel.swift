//
//  LoginViewModel.swift
//  Message
//
//  Created by Slava Bulgakov on 12/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Result

class LoginViewModel {
    let appController: AppController
    fileprivate let signInStatusObserver: Observer<Status, NoError>
    let signInStatusSignal: Signal<Status, NoError>
    fileprivate let alertObserver: Observer<ActionMenuModel, NoError>
    let alertSignal: Signal<ActionMenuModel, NoError>
    
    init(appController: AppController) {
        self.appController = appController
        (signInStatusSignal, signInStatusObserver) = Signal<Status, NoError>.pipe()
        (alertSignal, alertObserver) = Signal<ActionMenuModel, NoError>.pipe()
    }
    
    enum Status {
        case success
        case signUpNeeded
    }
    
    func signIn(login: String, password: String) {
        appController.network?.signIn(login: login, password: password)?.startWithResult({ [weak self] result in
            switch result {
            case .success:
                self?.signInStatusObserver.send(value: .success)
            case .failure(let error):
                self?.alertObserver.send(value: error.menuModel { [weak self] in self?.signInStatusObserver.send(value: .signUpNeeded) })
            }
        })
    }
}

fileprivate extension NetworkController.NetworkError {
    func menuModel(signUpNeededHandler: @escaping ()->()) -> ActionMenuModel {
        switch self {
        case .signUpNeeded:
            let actionMenuModel = ActionMenuModel(title: "Ошибка", message: "Данный логин не зарегестрирован. Хотите зарегистрироваться?")
            actionMenuModel.addAction(ActionMenuAction(title: "OK", style: .default, image: nil, enabled: true) { _ in
                signUpNeededHandler()
            })
            actionMenuModel.addAction(ActionMenuAction(title: "Cancel", style: .cancel, image: nil, enabled: true, handler: nil))
            return actionMenuModel
        case .wrongPassword:
            let actionMenuModel = ActionMenuModel(title: "Неверный пароль", message: nil)
            actionMenuModel.addAction(ActionMenuAction(title: "OK", style: .cancel, image: nil, enabled: true, handler: nil))
            return actionMenuModel
        default:
            let actionMenuModel = ActionMenuModel(title: "Неизвестная ошибка", message: nil)
            actionMenuModel.addAction(ActionMenuAction(title: "OK", style: .cancel, image: nil, enabled: true, handler: nil))
            return actionMenuModel
        }
    }
}
