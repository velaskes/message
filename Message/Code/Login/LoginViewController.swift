//
//  LoginViewController.swift
//  Message
//
//  Created by Slava Bulgakov on 12/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import UIKit
import Alamofire
import ReactiveSwift
import ReactiveCocoa

class LoginViewController: UIViewController, Coordinated {
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    var coordinationDelegate: CoordinationDelegate?
    
    fileprivate var disposable: ScopedDisposable<AnyDisposable>?
    
    var viewModel: LoginViewModel? {
        didSet { bindViewModel() }
    }

    func bindViewModel() {
        let composite = CompositeDisposable()
        defer {
            disposable = ScopedDisposable(composite)
        }
        
        guard let viewModel = self.viewModel else { return }
        
        composite += viewModel.signInStatusSignal.take(during: self.reactive.lifetime).observeValues { [weak self] status in
            switch status {
            case .success:
                self?.dismiss(animated: true, completion: nil)
            case .signUpNeeded:
                self?.performSegue(withIdentifier: "SignUp", sender: nil)
            }
        }
        
        composite += viewModel.alertSignal.take(during: self.reactive.lifetime).observeValues { [weak self] alertModel in
            let alert = UIAlertController(model: alertModel, preferresStyle: .alert)
            self?.present(alert, animated: true, completion: nil)
        }
    }

    @IBAction func loginButtonTap(_ sender: Any) {
        guard let login = loginField.text, let password = passwordField.text else { return }
        viewModel?.signIn(login: login, password: password)
    }
}
