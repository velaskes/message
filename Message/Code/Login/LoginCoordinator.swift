//
//  LoginCoordinator.swift
//  Message
//
//  Created by Slava Bulgakov on 14/05/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import UIKit

class LoginCoordinator: Coordinator {
    var appController: AppController
    let loginViewController: LoginViewController
    
    init(loginViewController: LoginViewController, appController: AppController) {
        self.loginViewController = loginViewController
        self.appController = appController
    }
    
    func start() {
        let viewModel = LoginViewModel(appController: appController)
        loginViewController.viewModel = viewModel
        loginViewController.coordinationDelegate = self
        
        viewModel.signInStatusSignal.observeValues { [weak self] status in
            guard case .signUpNeeded = status else { return }
            self?.loginViewController.performSegue(withIdentifier: "SignUp", sender: nil)
        }
    }
}

extension LoginCoordinator: CoordinationDelegate {
    func prepareForSegue(segue: UIStoryboardSegue) {
        guard let controller = segue.destination as? SignUpViewController else { return }
        SignUpCoordinator(signUpViewController: controller, appController: appController).start()
    }
}
