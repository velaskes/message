//
//  ChatsCoordinator.swift
//  Message
//
//  Created by Slava Bulgakov on 14/05/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class ChatsCoordinator: Coordinator {
    let chatsViewController: ChatsViewController
    var appController: AppController
    
    fileprivate let token: Lifetime.Token
    fileprivate let lifetime: Lifetime
    
    var selectedChat: Chat?

    init(chatsViewController: ChatsViewController, appController: AppController) {
        self.chatsViewController = chatsViewController
        self.appController = appController
        token = Lifetime.Token()
        lifetime = Lifetime(token)
    }

    func start() {
        let viewModel = ChatsViewModel()
        viewModel.appController = appController
        chatsViewController.viewModel = viewModel
        chatsViewController.coordinationDelegate = self
        chatsViewController.viewDidLoadSignal.flatMap(.latest) { [weak self] () -> SignalProducer<Void, NoError> in
            self?.appController.load()
            return self?.appController.session?.userProperty.producer.filter({ $0 == nil }).map({ _ in return () }) ?? SignalProducer.empty
            }.take(during: lifetime).observeValues { [weak self] _ in
                self?.chatsViewController.performSegue(withIdentifier: "Login", sender: nil)
        }
        
        viewModel.selectedChat.signal.skipNil().take(during: lifetime).observeValues { [weak self] chat in
            self?.selectedChat = chat
            self?.chatsViewController.performSegue(withIdentifier: "OpenChat", sender: nil)
        }
    }
}

extension ChatsCoordinator: CoordinationDelegate {
    func prepareForSegue(segue: UIStoryboardSegue) {
        var coordinator: Coordinator?
        var destination: UIViewController? = segue.destination
        if let d = destination as? UINavigationController {
            destination = d.topViewController
        }
        switch destination {
        case let controller as LoginViewController:
            coordinator = LoginCoordinator(loginViewController: controller, appController: appController)
        case let controller as ChatViewController:
            guard let chat = self.selectedChat else {
                print("There is no selected chat")
                return
            }
            coordinator = ChatCoordinator(chatViewController: controller, appController: appController, chat: chat)
        case let controller as CreateChatViewController:
            coordinator = CreateChatCoordinator(createChatViewController: controller, appController: appController)
        default:
            break
        }
        coordinator?.start()
    }
}
