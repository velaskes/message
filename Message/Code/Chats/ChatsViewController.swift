//
//  ChatsViewController.swift
//  Message
//
//  Created by Slava Bulgakov on 12/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class ChatsViewController: UIViewController, Coordinated {
    let cellIdentifier = "ChatCellIdentifier"
    @IBOutlet weak var tableView: UITableView!
    var coordinationDelegate: CoordinationDelegate?
    let (viewDidLoadSignal, viewDidLoadObserver) = Signal<Void, NoError>.pipe()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(loadChats), for: .valueChanged)
        viewDidLoadObserver.send(value: Void())
        bindViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadChats()
    }
    
    var viewModel: ChatsViewModel?
    
    func bindViewModel() {
        viewModel?.chats.producer.take(during: self.reactive.lifetime).startWithValues{ [weak self] _ in
            self?.tableView.refreshControl?.endRefreshing()
            self?.tableView.reloadData()
        }
    }
    
    func loadChats() {
        viewModel?.loadChats()
    }
}

extension ChatsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier), let viewModel = viewModel else {
            let cell = UITableViewCell()
            cell.backgroundColor = UIColor.red
            return cell
        }
        cell.set(viewModel: viewModel.item(atIndex: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.select(index: indexPath.row)
    }
}

extension UITableViewCell {
    func set(viewModel: ChatsCellViewModel) {
        textLabel?.text = viewModel.title
    }
}
