//
//  ChatViewModel.swift
//  Message
//
//  Created by Slava Bulgakov on 23/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Result

class ChatsViewModel {
    var appController: AppController?
    fileprivate let token: Lifetime.Token
    fileprivate let lifetime: Lifetime
    
    fileprivate let _chats = MutableProperty<[ChatsCellViewModel]>([ChatsCellViewModel]())
    fileprivate(set) lazy var chats: Property<[ChatsCellViewModel]> = {
        return Property(self._chats)
    }()
    
    fileprivate var _selectedChat = MutableProperty<Chat?>(nil)
    fileprivate(set) lazy var selectedChat: Property<Chat?> = {
        return Property(self._selectedChat)
    }()
    
    init() {
        token = Lifetime.Token()
        lifetime = Lifetime(token)
    }
    
    func loadChats() {
        appController?.network?.chats().take(during: lifetime).startWithResult({ [weak self] result in
            guard case .success(let chats) = result else { return }
            self?._chats.value = chats.chats.map({ ChatsCellViewModel(chat: $0) })
        })
    }
    
    var numberOfRows: Int {
        return self.chats.value.count
    }
    
    func item(atIndex index: Int) -> ChatsCellViewModel {
        return chats.value[index]
    }
    
    func select(index: Int) {
        _selectedChat.value = _chats.value[index].chat
    }
}
