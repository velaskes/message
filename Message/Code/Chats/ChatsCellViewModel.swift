//
//  ChatsCellViewModel.swift
//  Message
//
//  Created by Slava Bulgakov on 25/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation

class ChatsCellViewModel {
    let chat: Chat
    
    init(chat: Chat) {
        self.chat = chat
    }
    
    var title: String {
        return chat.members.map({ $0.login }).joined(separator: ", ")
    }
}
