//
//  Models.swift
//  MessageServer
//
//  Created by Slava Bulgakov on 06/11/2017.
//
//

import Foundation

class ServerUser: User {
    var md5Password: String!
    var accessToken: String!
    var refreshToken: String!
    var accessTokenExpiry: Date!
    var refreshTokenExpiry: Date!
    
    init(login: String, md5Password: String) {
        super.init(login: login)
        self.md5Password = md5Password
        refreshTokens()
    }
    
    func refreshTokens() {
        let date = Date()
        accessToken = NSUUID().uuidString
        accessTokenExpiry = date
        refreshToken = NSUUID().uuidString
        refreshTokenExpiry = date
    }
    
    fileprivate func isAccessTokenFresh(token: String) -> Bool {
        return accessToken == token && accessTokenExpiry < Date()
    }
    
    fileprivate func isRefreshTokenFresh(token: String) -> Bool {
        return refreshToken == token && refreshTokenExpiry < Date()
    }
}


class Users {
    var users = [ServerUser]()
    var lastUserId = 0
    
    func add(_ user: ServerUser) {
        users += [user]
        lastUserId += 1
    }
    
    func user(byLogin login: String) -> ServerUser? {
        return users.filter({ $0.login == login }).first
    }
    
    func user(byAccessToken token: String) -> ServerUser? {
        return users.first(where: { $0.isAccessTokenFresh(token: token) })
    }
    
    func user(byRefreshToken token: String) -> ServerUser? {
        return users.first(where: { $0.isRefreshTokenFresh(token: token) })
    }
}
