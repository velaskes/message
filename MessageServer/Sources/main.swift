import Vapor
import Foundation
import HTTP
import Sessions

class WebSocketChat {
    let chat: Chat
    var sockets = [String: WebSocket]()
    
    init(chat: Chat) {
        self.chat = chat
    }
}

var lastRoomId = 0
var users = Users()
var chats = [WebSocketChat]()

func response(command: Command) throws -> Response {
    do {
        let data = try commandFamilyRule.dumpData(command)
        return Response(status: .ok, headers: ["Content-Type": "application/json"], body: .data(data.bytes))
    } catch {
        print(error)
        throw MessageAbort(status: .serverError)
    }
}

extension Data {
    var bytes: Bytes {
        return withUnsafeBytes { Array(UnsafeBufferPointer<Byte>(start: $0, count: count))}
    }
}

func user(byRequest request: Request) throws -> ServerUser {
    guard let accessToken = request.headers["accessToken"], let user = users.user(byAccessToken: accessToken)
        else {
            throw MessageAbort(status: .wrongAccessToken)
    }
    return user
}

func command<T: Command>(byRequest request: Request) throws -> T {
    if let bytes = request.body.bytes,
        let command = (try? commandFamilyRule.validateData(Data(bytes: bytes))) as? T {
        return command
    }
    throw MessageAbort(status: .serverError)
}

do {
    let config = try Config()
    config.addConfigurable(middleware: MessageErrorMiddleware(), name: "MessageErrorMiddleware")
    let drop = try Droplet(config)
    
    drop.group("v1.0") { routeGroup in
        routeGroup.post(Endpoint.signIn.rawValue) { request in
            let credentials: Credentials = try command(byRequest: request)
            guard let user = users.user(byLogin: credentials.login) else { throw MessageAbort(status: .signUpNeeded) }
            guard user.md5Password == credentials.md5Password else { throw MessageAbort(status: .wrongPassword) }
            user.refreshTokens()
            return try response(command: Tokens(accessToken: user.accessToken, refreshToken: user.refreshToken))
        }
        
        routeGroup.post(Endpoint.refreshToken.rawValue) { request in
            let refreshToken: RefreshToken = try command(byRequest: request)
            guard let user = users.user(byRefreshToken: refreshToken.refreshToken)
                else { throw MessageAbort(status: .signInNeeded) }
            user.refreshTokens()
            return try response(command: Tokens(accessToken: user.accessToken, refreshToken: user.refreshToken))
        }
        
        routeGroup.post(Endpoint.signUp.rawValue) { request in
            let credentials: Credentials = try command(byRequest: request)
            if let _ = users.user(byLogin: credentials.login) { throw MessageAbort(status: .loginIsAlreadyExist) }
            let user = ServerUser(login: credentials.login, md5Password: credentials.md5Password)
            users.add(user)
            return try response(command: Tokens(accessToken: user.accessToken, refreshToken: user.refreshToken))
        }
        
        routeGroup.get(Endpoint.createChat.rawValue) { request in
            guard let targetUserLogin = request.query?["user_login"]?.string,
                let targetUser = users.user(byLogin: targetUserLogin)
                else { return try response(command: ErrorResponse(errorType: .loginDontExist)) }
            let chat = Chat(id: lastRoomId)
            lastRoomId += 1
            chat.members += [try user(byRequest: request), targetUser]
            chats += [WebSocketChat(chat: chat)]
            return try response(command: chat)
        }
        
        routeGroup.get(Endpoint.chats.rawValue) { request in
            let c = Chats()
            let u = try user(byRequest: request)
            c.chats = chats.filter{ $0.chat.members.contains(u) }.map{ return $0.chat }
            return try response(command: c)
        }
        
        routeGroup.socket("ws") { request, ws in
            print("New WebSocket connected: \(ws)")
            
            background {
                while ws.state == .open {
                    try? ws.ping()
                    drop.console.wait(seconds: 10) // every 10 seconds
                }
            }

            ws.onBinary = { ws, bytes in
                let data = Data(bytes: bytes)
                guard let command = try? commandFamilyRule.validateData(data) else { return }
                switch command {
                case let message as Message:
                    let chat = chats.first(where: { $0.chat.id == message.chatId })
                    chat?.sockets.forEach({ (_, ws) in
                        try? ws.send(data.bytes)
                    })
                    break
                case let enter as Enter:
                    let chat = chats.first(where: { $0.chat.id == enter.chatId })
                    chat?.sockets[enter.userLogin] = ws
                default:
                    print("Wrong message")
                    break
                }
            }
            
            ws.onClose = { ws, code, reason, clean in
                print("Closed.")
            }
        }
    }
    try drop.run()
} catch {
    print("Cannot run server: \(error)")
}

