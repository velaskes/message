//
//  ErrorMiddleware.swift
//  MessageServer
//
//  Created by Slava Bulgakov on 09/11/2017.
//
//

import HTTP

struct MessageAbort: Error {
    let status: ErrorType
}

final class MessageErrorMiddleware: Middleware {
    func respond(to request: Request, chainingTo next: Responder) throws -> Response {
        do {
            return try next.respond(to: request)
        } catch let error as MessageAbort {
            return try response(command: ErrorResponse(errorType: error.status))
        }
    }
}
