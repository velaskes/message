//
//  JSONModels.swift
//  MessageServer
//
//  Created by Slava Bulgakov on 03/03/2017.
//
//

import Foundation

#if Mobile
import Bender
#endif

func val<T>(_ closure: @autoclosure @escaping ()throws -> T) -> T? {
    do {
        return try closure()
    } catch {
        print("\(error)")
        return nil
    }
}

public enum CommandType: String {
    case enter = "enter"
    case message = "message"
    case error = "error"
    case chat = "chat"
    case chats = "chats"
    case user = "user"
    case credentials = "credentials"
    case tokens = "tokens"
    case refreshToken = "refreshToken"
    
    public static var rule: EnumRule<CommandType> {
        return EnumRule<CommandType>()
            .option(CommandType.enter.rawValue, .enter)
            .option(CommandType.message.rawValue, .message)
            .option(CommandType.error.rawValue, .error)
            .option(CommandType.chat.rawValue, .chat)
            .option(CommandType.chats.rawValue, .chats)
            .option(CommandType.user.rawValue, .user)
            .option(CommandType.credentials.rawValue, .credentials)
            .option(CommandType.tokens.rawValue, .tokens)
            .option(CommandType.refreshToken.rawValue, .refreshToken)
    }
}

protocol Command: class, CustomStringConvertible {
    var type: CommandType! { get set }
}

extension Command {
    public var description: String {
        return "\(type(of: self))"
    }
    
    static func commandRule<T: Command>(rule: ClassRule<T>) -> ClassRule<T> {
        return rule.expect("type", CommandType.rule, { $0.type = $1 }) { $0.type }
    }
}

public class Message: Command {
    public var type: CommandType! = .message
    public var text: String!
    public var chatId: Int!
    public var userLogin: String!
    
    public static var rule: ClassRule<Message> = commandRule(rule: ClassRule(Message()))
        .required("type", CommandType.rule) { $0 == .message }
        .expect("text", StringRule, { $0.text = $1 }) { $0.text }
        .expect("chatId", IntRule, { $0.chatId = $1 }) { $0.chatId }
        .expect("userLogin", StringRule, { $0.userLogin = $1 }) { $0.userLogin }
}

public class Enter: Command {
    public var type: CommandType! = .enter
    public var chatId: Int!
    public var userLogin: String!
    
    public static var rule = commandRule(rule: ClassRule(Enter()))
        .required("type", CommandType.rule) { $0 == .enter }
        .expect("chatId", IntRule, { $0.chatId = $1 }) { $0.chatId }
        .expect("userLogin", StringRule, { $0.userLogin = $1 }) { $0.userLogin }
}

public enum ErrorType: String {
    case signUpNeeded = "signUpNeeded"
    case signInNeeded = "signInNeeded"
    case wrongPassword = "wrongPassword"
    case wrongAccessToken = "wrongAccessToken"
    case loginDontExist = "loginDontExist"
    case loginIsAlreadyExist = "loginIsAlreadyExist"
    case serverError = "serverError"
    case unknownError = "unknownError"
    
    public static var rule: EnumRule<ErrorType> {
        return EnumRule<ErrorType>()
            .option(ErrorType.signUpNeeded.rawValue, .signUpNeeded)
            .option(ErrorType.wrongPassword.rawValue, .wrongPassword)
            .option(ErrorType.wrongAccessToken.rawValue, .wrongAccessToken)
            .option(ErrorType.loginDontExist.rawValue, .loginDontExist)
            .option(ErrorType.loginIsAlreadyExist.rawValue, .loginIsAlreadyExist)
            .option(ErrorType.serverError.rawValue, .serverError)
            .option(ErrorType.unknownError.rawValue, .unknownError)
    }
}

public class ErrorResponse: Command {
    var type: CommandType! = .error
    var errorType: ErrorType
    
    init(errorType: ErrorType) {
        self.errorType = errorType
    }
    
    public static var rule = commandRule(rule: ClassRule(ErrorResponse(errorType: .unknownError)))
        .required("type", CommandType.rule) { $0 == .error }
        .expect("errorType", ErrorType.rule, { $0.errorType = $1 }) { $0.errorType }
    
    public var description: String {
        return "\(type(of: self)) { type: \(type.rawValue), errorType: \(errorType) }"
    }
}

public class User: Command, Equatable {
    var type: CommandType! = .user
    var login: String!
    
    init() {}
    init(login: String) {
        self.login = login
    }
    
    public static var rule = commandRule(rule: ClassRule(User()))
        .required("type", CommandType.rule) { $0 == .user }
        .expect("login", StringRule, { $0.login = $1 }) { $0.login }
    
    public var description: String {
        return "\(type(of: self)) { type: \(type.rawValue), login: \(login) }"
    }
    
    public static func ==(lhs: User, rhs: User) -> Bool {
        return lhs.login == rhs.login
    }
}

public class Credentials: Command {
    var type: CommandType! = .credentials
    var login: String!
    var md5Password: String!
    
    init() {}
    init(login: String, md5Password: String) {
        self.login = login
        self.md5Password = md5Password
    }
    
    public static var rule = commandRule(rule: ClassRule(Credentials()))
        .required("type", CommandType.rule) { $0 == .credentials }
        .expect("md5Password", StringRule, { $0.md5Password = $1 }) { $0.md5Password }
        .expect("login", StringRule, { $0.login = $1 }) { $0.login }
    
    public var description: String {
        return "\(type(of: self)) { type: \(type.rawValue), md5Password: \(md5Password), login: \(login) }"
    }
}

public class Tokens: Command {
    var type: CommandType! = .tokens
    var accessToken: String!
    var refreshToken: String!
    
    init() {}
    init(accessToken: String, refreshToken: String) {
        self.accessToken = accessToken
        self.refreshToken = refreshToken
    }
    
    public static var rule = commandRule(rule: ClassRule(Tokens()))
        .required("type", CommandType.rule) { $0 == .tokens }
        .expect("accessToken", StringRule, { $0.accessToken = $1 }) { $0.accessToken }
        .expect("refreshToken", StringRule, { $0.refreshToken = $1 }) { $0.refreshToken }
    
    public var description: String {
        return "\(type(of: self)) { type: \(type.rawValue), accessToken: \(accessToken), refreshToken: \(refreshToken) }"
    }
}

public class RefreshToken: Command {
    var type: CommandType! = .refreshToken
    var refreshToken: String!
    
    init() {}
    init(refreshToken: String) {
        self.refreshToken = refreshToken
    }
    
    public static var rule = commandRule(rule: ClassRule(RefreshToken()))
        .required("type", CommandType.rule) { $0 == .refreshToken }
        .expect("refreshToken", StringRule, { $0.refreshToken = $1 }) { $0.refreshToken }
    
    public var description: String {
        return "\(type(of: self)) { type: \(type.rawValue), refreshToken: \(refreshToken) }"
    }
}

public class Chat: Command {
    var type: CommandType! = .chat
    var id: Int!
    var members = [User]()
    
    init(id: Int) {
        self.id = id
    }
    
    init() {}
    
    public static var rule = commandRule(rule: ClassRule(Chat()))
        .required("type", CommandType.rule) { $0 == .chat }
        .expect("id", IntRule, { $0.id = $1 }) { $0.id }
        .expect("members", ArrayRule(itemRule: User.rule), { $0.members = $1 }) { $0.members }
    
    public var description: String {
        return "\(type(of: self)) { type: \(type.rawValue), id: \(id) }"
    }
}

public class Chats: Command {
    var type: CommandType! = .chats
    var chats = [Chat]()
    
    public static var rule = commandRule(rule: ClassRule(Chats()))
        .required("type", CommandType.rule) { $0 == .chats }
        .expect("chats", ArrayRule(itemRule: Chat.rule), { $0.chats = $1 }) { $0.chats }
    
    public var description: String {
        return "\(type(of: self)) { type: \(type.rawValue) }"
    }
}

class CommandFamilyRule: Rule {
    typealias V = Command
    
    private let message = Message.rule
    private let enter = Enter.rule
    private let error = ErrorResponse.rule
    private let chat = Chat.rule
    private let user = User.rule
    private let chats = Chats.rule
    private let credentials = Credentials.rule
    private let tokens = Tokens.rule
    private let refreshToken = RefreshToken.rule
    
    func validate(_ jsonValue: AnyObject) throws -> V {
        var value: Command? = val(try self.message.validate(jsonValue)) ??
            val(try self.enter.validate(jsonValue)) ??
            val(try self.error.validate(jsonValue)) ??
            val(try self.chat.validate(jsonValue)) ??
            val(try self.user.validate(jsonValue)) ??
            val(try self.refreshToken.validate(jsonValue))
        value = value ??
            val(try self.chats.validate(jsonValue)) ??
            val(try self.tokens.validate(jsonValue)) ??
            val(try self.credentials.validate(jsonValue))
        guard let result = value else {
            let message = "\(V.self) of unexpected format found: \"\(jsonValue)\""
            print(message)
            throw RuleError.invalidJSONType(message, nil)
        }
        return result
    }
    
    func dump(_ value: V) throws -> AnyObject {
        switch value {
        case let input as Message:
            return try message.dump(input)
        case let input as Enter:
            return try enter.dump(input)
        case let input as ErrorResponse:
            return try error.dump(input)
        case let input as Chat:
            return try chat.dump(input)
        case let input as User:
            return try user.dump(input)
        case let input as Chats:
            return try chats.dump(input)
        case let input as Credentials:
            return try credentials.dump(input)
        case let input as Tokens:
            return try tokens.dump(input)
        case let input as RefreshToken:
            return try refreshToken.dump(input)
        default:
            throw RuleError.invalidDump("Unknown type \(type(of: self)) in CommandFamilyRule", nil)
        }
    }
}

let commandFamilyRule = CommandFamilyRule()
