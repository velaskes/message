//
//  Common.swift
//  Message
//
//  Created by Slava Bulgakov on 26/03/2017.
//  Copyright © 2017 Slava Bulgakov. All rights reserved.
//

import Foundation

enum Endpoint: String {
    case chats = "chats"
    case createChat = "create_chat"
    case signIn = "sign_in"
    case signUp = "sign_up"
    case refreshToken = "refresh_token"
}
